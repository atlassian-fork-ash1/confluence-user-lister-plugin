package com.atlassian.confluence.extra.userlister;

import java.util.Set;


public interface UserListManager {
    static String BANDANA_KEY_BLACK_LIST = "com.atlassian.confluence.extra.userlister.blacklist";

    Set<String> getGroupBlackList();

    void saveGroupBlackList(Set<String> deniedGroups);

    boolean isGroupPermitted(final String groupName);

    Set<String> getLoggedInUsers();

    void registerLoggedInUser(String userName, String sessionId);

    void unregisterLoggedInUser(String userName, String sessionId);
}
